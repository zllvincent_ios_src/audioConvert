//
//  ZLLVoiceTools.h
//  AmrWavConverter
//
//  Created by Vincent on 2017/2/7.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import <Foundation/Foundation.h>



/*ios 音频格式caf、wav不支持android的amr格式，需要转换
 *该类为iOS音频工具类
 *
 *
 */

@interface ZLLVoiceTools : NSObject

/**
 *  转换wav到amr
 *
 *  @param aWavPath  wav文件路径
 *  @param aSavePath amr保存路径
 *
 *  @return 0失败 1成功
 */
+ (int)convertWavToAmr:(NSString *)aWavPath amrSavePath:(NSString *)aSavePath;

/**
 *  转换amr到wav
 *
 *  @param aAmrPath  amr文件路径
 *  @param aSavePath wav保存路径
 *
 *  @return 0失败 1成功
 */
+ (int)convertAmrToWav:(NSString *)aAmrPath wavSavePath:(NSString *)aSavePath;

@end
