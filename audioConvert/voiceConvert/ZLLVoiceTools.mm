//
//  ZLLVoiceTools.m
//  AmrWavConverter
//
//  Created by Vincent on 2017/2/7.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import "ZLLVoiceTools.h"
#import "amrFileCodec.h"

@implementation ZLLVoiceTools




//转换amr到wav
+ (int)convertAmrToWav:(NSString *)aAmrPath wavSavePath:(NSString *)aSavePath{
    
    if (! DecodeAMRFileToWAVEFile([aAmrPath cStringUsingEncoding:NSASCIIStringEncoding], [aSavePath cStringUsingEncoding:NSASCIIStringEncoding])){
        return 0;
    }
    
    return 1;
}

//转换wav到amr
+ (int)convertWavToAmr:(NSString *)aWavPath amrSavePath:(NSString *)aSavePath{
    
    if (! EncodeWAVEFileToAMRFile([aWavPath cStringUsingEncoding:NSASCIIStringEncoding], [aSavePath cStringUsingEncoding:NSASCIIStringEncoding], 1, 16)){
        return 0;
    }
    return 1;
}


@end
