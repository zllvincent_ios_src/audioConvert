//
//  ZLLRecordingView.m
//  AudioRecorder
//
//  Created by Vincent on 2017/2/6.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import "ZLLRecordingView.h"


@interface ZLLRecordingView ()<AVAudioRecorderDelegate>

//录音音高视图
@property(nonatomic,strong)UIImageView *imgV;
@property(nonatomic,strong)AVAudioRecorder *recorder;
@property(nonatomic,strong)NSTimer *timer;


@end

@implementation ZLLRecordingView

//开始录音
-(NSError *)recordVoiceWithURL:(NSURL *)aURL{
    
    self.imgV.frame = self.bounds;
    NSString *bundle = [[NSBundle mainBundle] pathForResource:@"ZLLRecord" ofType:@"bundle"];
    [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_01.png"]]];
    self.imgV.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.imgV];
    
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *sessionError = nil;
    //AVAudioSessionCategoryPlayAndRecord用于录音和播放
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    if(sessionError){
        return sessionError;
    }
    [session setActive:YES error:nil];
    
    //录音设置
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc]init];
    //设置录音格式  AVFormatIDKey==kAudioFormatLinearPCM
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    //设置录音采样率(Hz) 如：AVSampleRateKey==8000/44100/96000（影响音频的质量）
    [recordSetting setValue:[NSNumber numberWithFloat:8000] forKey:AVSampleRateKey];
    //录音通道数  1 或 2
    [recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
    //线性采样位数  8、16、24、32
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    //录音的质量
    [recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
    
    
    NSError *error = nil;
    //初始化
    self.recorder = [[AVAudioRecorder alloc]initWithURL:aURL settings:recordSetting error:&error];
    //开启音量检测
    if(error){
        return error;
    }
    
    self.recorder.meteringEnabled = YES;
    self.recorder.delegate = self;
    
    if([self.recorder prepareToRecord]){
        [self.recorder record];
        
    }

    
    //更新音高视图
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(detectionVoice) userInfo:nil repeats:YES];
    
    
    return nil;
}

-(UIImageView *)imgV{
    if(_imgV==nil){
        _imgV = [UIImageView new];
    }
    return _imgV;
}


//定时检测声音
-(void)detectionVoice{
    
    [self.recorder updateMeters];//刷新音量数据
    //获取音量的平均值  [recorder averagePowerForChannel:0];
    //音量的最大值  [recorder peakPowerForChannel:0];
    
    double lowPassResults = pow(10, (0.05 * [self.recorder peakPowerForChannel:0]));

    
    //最大50  0
    //图片 小-》大
    NSString *bundle = [[NSBundle mainBundle] pathForResource:@"ZLLRecord" ofType:@"bundle"];
    if (0<lowPassResults<=0.06) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_01.png"]]];
    }
    else if (0.06<lowPassResults<=0.13) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_02.png"]]];
    }
    else if (0.13<lowPassResults<=0.20) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_03.png"]]];
    }
    else if (0.20<lowPassResults<=0.27) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_04.png"]]];
    }
    else if (0.27<lowPassResults<=0.34) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_05.png"]]];
    }
    else if (0.34<lowPassResults<=0.41) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_06.png"]]];
    }
    else if (0.41<lowPassResults<=0.48) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_07.png"]]];
    }
    else if (0.48<lowPassResults<=0.55) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_08.png"]]];
    }
    else if (0.55<lowPassResults<=0.62) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_09.png"]]];
    }
    else if (0.62<lowPassResults<=0.69) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_10.png"]]];
    }
    else if (0.69<lowPassResults<=0.76) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_11.png"]]];
    }
    else if (0.76<lowPassResults<=0.83) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_12.png"]]];
    }
    else if (0.83<lowPassResults<=0.9) {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_13.png"]]];
    }
    else {
        [self.imgV setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",bundle,@"zll_record_animate_14.png"]]];
    }
}

//停止录音
-(void)stopRecordingWithBlock:(void (^)(NSURL *))block{
    
    [self.timer invalidate];
    self.timer = nil;
    
    [self.recorder stop];
    
    if(block){
        block(self.recorder.url);
    }
}


-(void)dealloc{
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
