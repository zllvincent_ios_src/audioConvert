//
//  ZLLRecordingView.h
//  AudioRecorder
//
//  Created by Vincent on 2017/2/6.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

/*
 *录音视图（ios不支持mp3格式的录音,ios录音格式通常为wav。android默认录音文件格式为amr。两端需要进行转码才能正常使用)
 *
 */
@interface ZLLRecordingView : UIView


/**
 *开始录音
 *@param aURL   录音文件路径(文件类型扩展名以wav为宜）
 *
 */
-(NSError *)recordVoiceWithURL:(NSURL *)aURL;


/**
 *结束录音
 *@param block  停止录音的回调
 *
 */
-(void)stopRecordingWithBlock:(void (^)(NSURL *aURL))block;



@end
